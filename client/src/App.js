import React, { useEffect, useState } from 'react';
import './Styles/App.css';
import AppRouter from './Routes/AppRouter';
import { AuthContext } from './context/index';
import axios from 'axios';
import myAxi from './http/interceptor';


function App() {

  const [isAuth, setIsAuth] = useState(false);
  const [isLoad, setIsLoad] = useState(true);
  const [isUser, setIsUser] = useState();

  useEffect(() => {
    axios.get('https://localhost:5000/api/order', { withCredentials: true })
      .then(res => {
        myAxi.get(`/rating/${res.data.id}`)
          .then(result => {
            setIsUser({...result.data[0], role: res.data.role});
            setIsAuth(true);
            setIsLoad(false);
          })
      })
      .catch(err => {
        console.error(err);
        setIsLoad(false);
      })
  }, []);

  return (
    <AuthContext.Provider value={{
      isAuth,
      setIsAuth,
      isLoad,
      isUser,
      setIsUser
    }}>
      <AppRouter />
    </AuthContext.Provider>
  );
};

export default App;
