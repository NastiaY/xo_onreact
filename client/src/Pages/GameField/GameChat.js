import { makeAutoObservable } from 'mobx';
import { makePersistable } from "mobx-persist-store";


class GameChat {

    messages = [];

    constructor() {
        makeAutoObservable(this);
    };

    setMessages(mess) {
        this.messages = mess;
    };

    sendMessage(msg) {
        this.messages = [...this.messages, msg];
    };

    newChat() {
        this.messages = [];
    };

};

export default new GameChat();