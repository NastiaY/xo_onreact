import { action, makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";
import Wsservice from '../../http/WebSocketService';

class Game {

    opponent = null;
    mode = 'x';
    field = [null, null, null, null, null, null, null, null, null];
    winComb = [null];
    users = [];
    session = null;
    rolePl = {};
    allUsersOnline = [];

    constructor() {

        makeAutoObservable(this, {
            isWinComb: action,
        });

        makePersistable(this, {
            name: 'GameStore',
            properties: ['field', 'session', 'rolePl', 'opponent'],
            storage: window.localStorage,
            // expireIn: 86400000 * 30,
        });
    };

    get winner() {
        return ['x', 'o'].find((symb) => this.isWinner(symb)) ?? null;
    };

    get isDraw() {
        return !this.winner && !this.field.some((cell) => !cell);
    };

    get isOver() {
        return this.winner || !this.field.some((cell) => !cell);
    };

    async makeMove(index) {
        if (this.field[index]) return console.log('Клетка занята');
        this.field[index] = this.mode;
        await Wsservice.CaseGame(index, this.mode, this.session);
    };

    moveMade(index, symb) {
        this.field[index] = symb;
        this.mode = (symb === 'x') ? 'o' : 'x';
    };

    setField(field) {
        this.field = field;
        let symbX = this.field.filter(symb => symb === 'x').length;
        let symbO = this.field.filter(symb => symb === 'o').length;
        this.mode = symbO < symbX ? 'o' : 'x';
    };

    isWinner(symb) {
        const Comb = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
        return Comb.some((combin) => {
            if (combin.every((ind) => {
                return this.field[ind] === symb;})) {
                this.isWinComb(combin);
                return true;
            } else {
                this.isWinComb([null]);
                return false;
            };
        });
    };

    isWinComb(comb) {
        this.winComb = comb;
    };

    newGame() {
        this.field = [null, null, null, null, null, null, null, null, null];
        this.mode = 'x';
        this.winComb = [null];
        this.opponent = null;
        this.rolePl = {};
        this.session = null;
    };

    setUsers(data) {
        this.users = data;
    };

    setSessionId(id) {
        this.session = id;
    };

    setRolepl(obj) {
        this.rolePl = obj; 
    };

    setOpponent(id) {
        this.opponent = id;
    };

    setUserOnline(data) {
        this.allUsersOnline = data;
    };
};

export default new Game();