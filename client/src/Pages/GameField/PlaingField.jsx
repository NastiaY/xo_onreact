import React, { useState, useEffect, useContext } from 'react';
import Omini from '../../components/UI/XOmedia/Omini.svg';
import Xmini from '../../components/UI/XOmedia/Xmini.svg';
import PlayersInGame from '../Modals/PlayersInGame';
import cl from './Field.module.css';
import Winner from '../Modals/Winner';
import ChatForm from '../../components/ChatForm';
import Field from '../../components/Field';
import TimerGame from '../../components/UI/timer/TimerGame';
import { observer } from 'mobx-react-lite';
import Game from './GameClass';
import { AuthContext } from '../../context/index';
import WSservice from '../../http/WebSocketService';
import myAxi from '../../http/interceptor';
import Serv from '../../http/servService';


const PlaingField = () => {

    const { isUser } = useContext(AuthContext);
    const [giveUp, setGiveUp] = useState(false);

    useEffect(() => {
        if (Game.session) {
            myAxi.get(`/rating/${Game.opponent}`)
                .then(res => {
                    Game.setUsers([isUser, res.data[0]]);
                    Serv.getChat(Game.session);
                    Serv.getGame(isUser.id);
                })
                .catch(err => {
                    console.error(err);
                })
        }
    }, [isUser])

    async function fold(e) {
        e.preventDefault();
        let symb = Game.rolePl['x'] === isUser.id ? 'o' : 'x';
        let newField = Game.field.map(el => el === null ? symb : el);
        WSservice.CaseGiveUp(newField);
        Serv.updateField(Game.session, newField);
    };


    return (
        Game.session
            ?
            <main className={cl.fonGame}>

                <Winner />

                <div className={cl.gField}>

                    <PlayersInGame />

                    <div className={cl.Board}>

                        <TimerGame />

                        <Field />

                        <form style={{ alignItems: 'center' }}>
                            <button className={cl.move}
                                onClick={e => fold(e)}
                                onMouseOver={e => setGiveUp(true)}
                                onMouseOut={e => setGiveUp(false)}>
                                {giveUp ?
                                    'СДАТЬСЯ' :
                                    <>Ходит
                                        <img src={Game.mode === 'x' ? Xmini : Omini} alt='игрок' />
                                        {Game.users.map(pl => {
                                            if (pl.id === Game.rolePl[Game.mode]) {
                                                return pl.firstName + ' ' + pl.surName
                                            }
                                        })}</>
                                }
                            </button>
                        </form>

                    </div>

                    <ChatForm />

                </div>
            </main>
            :
            <main className={cl.fonGame}>

                <div className={cl.Board}>

                    <Field disabled />

                </div>
            </main>
    );
};

export default observer(PlaingField);
