import React, { useState, useContext, useEffect } from 'react';
import cl from './Header.module.css';
import Logo from '../../components/UI/XOmedia/Exclude.svg';
import Out from '../../components/UI/XOmedia/Vector.svg';
import OutHover from '../../components/UI/XOmedia/closeHover.svg';
import { AuthContext } from '../../context/index';
import { useNavigate } from 'react-router-dom';
import Game from '../GameField/GameClass';
import WSservice from '../../http/WebSocketService';
import Chat from '../GameField/GameChat';
import myAxi from '../../http/interceptor';
import Serv from '../../http/servService';
import Timer from '../../components/UI/timer/TimerField';
import { observer } from 'mobx-react-lite';
import esc from '../../components/UI/XOmedia/Esc.svg';
import NavBar from './NavBar';
import ham from '../../components/UI/XOmedia/hamburger.svg';


const Header = () => {

    const { isUser, setIsUser, setIsAuth } = useContext(AuthContext);
    const router = useNavigate();
    const [valueImg, setValueImg] = useState(Out);

    useEffect(() => {
        WSservice.connect();
        WSservice.socket.onopen = () => {
            WSservice.handleOpen(isUser);
            if (Game.session) {
                WSservice.handleOpenGame(Game.session);
            };
        };
        WSservice.socket.onmessage = (event) => {
            let msg = JSON.parse(event.data);
            switch (msg.method) {
                case 'invite':
                    let answer = window.confirm(`Игрок ${msg.name} пригласил вас в игру`);
                    if (answer) {
                        Serv.updatePlayer(isUser.id, false);
                        WSservice.CaseInvite(msg)
                            .then(() => {
                                WSservice.handleOpenGame(Game.session);
                                router(`/plaingfield/${Game.session}`);
                            });
                    } else {
                        WSservice.CaseNo(msg, isUser);
                    };
                    break;
                case 'answer':
                    WSservice.CaseAnswer(msg);
                    WSservice.handleOpenGame(msg.gameSes);
                    Serv.updatePlayer(isUser.id, false);
                    router(`/plaingfield/${msg.gameSes}`);
                    break;
                case 'game':
                    Game.moveMade(msg.fieldIndex, msg.mode);
                    break;
                case 'chat':
                    Chat.sendMessage(msg);
                    break;
                case 'no':
                    alert(msg.message);
                    break;
                case 'connect':
                    Game.setUserOnline(msg.players);
                    break;
                case 'disconnect':
                    Game.setUserOnline(msg.players);
                    break;
                case 'giveup':
                    Game.setField(msg.field);
                    break;
                default:
                    break;
            };
        };
        WSservice.socket.onerror = (event) => {
            console.error(event);
        };
        return () => {
            WSservice.disconnect(isUser.id);
        };
    }, []);

    useEffect(() => {
        if (Game.isOver && !Timer.endGame) {
            Serv.updatePlayer(isUser.id, true);
            Timer.setEndGame(Date.now());
            clearInterval(Timer._job);
        }
    }, [Game.isOver])

    useEffect(() => {
        if (Game.session && !Game.isOver) {
            Timer.startTimer();
        } return () => clearInterval(Timer._job);
    }, [Game.mode]);

    useEffect(() => {
        if (!Timer.timerO && !Game.isOver) {
            if (Game.rolePl['x'] === isUser.id) {
                let newField = Game.field.map(el => el === null ? 'x' : el);
                timeIsOver(newField);
            };
        } else if (!Timer.timerX && !Game.isOver) {
            if (Game.rolePl['o'] === isUser.id) {
                let newField = Game.field.map(el => el === null ? 'o' : el);
                timeIsOver(newField);
            };
        };
    }, [Timer.timerO, Timer.timerX])

    async function timeIsOver(field) {
        await WSservice.CaseGiveUp(field);
        await Serv.updateField(Game.session, field);
        await Serv.updateGame(Date.now());
    };

    async function LogOut(e) {
        e.preventDefault();
        await myAxi.post('/logout')
            .then(res => {
                localStorage.clear();
                setIsUser({});
                setIsAuth(false);
                router('/');
            })
            .catch(err => {
                console.error(err);
            });
    };

    const [openMenu, setMenu] = useState(true);

    return (
        <header>

            <div className={cl.menuph}>
                <img src={Logo} alt='Лого' />
                <div className={cl.hamburger}>
                    <img src={openMenu ? ham : esc} alt="Закрыть"
                        onClick={e => setMenu(!openMenu)} />
                </div>
            </div>

            <div className={openMenu ? [cl.headerPC, cl.expand].join(' ') : cl.headerPC}>

                <div className={cl.menupc}>
                    <img src={Logo} alt='Лого' /> 
                </div>

                <NavBar stateMenu={setMenu} />

                <form style={{ width: 'max-content' }} onClick={LogOut}>
                    <button className={cl.signout}>
                        <img src={valueImg}
                            onMouseOver={e => setValueImg(OutHover)}
                            onMouseOut={e => setValueImg(Out)}
                            alt='Выход' />
                    </button>
                </form>

            </div>
        </header>
    );
};

export default observer(Header);
