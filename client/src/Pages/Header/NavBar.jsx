import React from 'react';
import cl from './Header.module.css';
import BtnMenu from '../../components/UI/buttons/BtnMenu';
import Game from '../GameField/GameClass';


const NavBar = ({stateMenu}) => {

    const pathField = Game.session ? `/plaingfield/${Game.session}` : '/plaingfield';

    return (
        <div className={cl.navBar} onClick={() => stateMenu(true)}>
            <BtnMenu to={pathField}>Игровое&nbsp;поле</BtnMenu>
            <BtnMenu to='/ratings'>Рейтинг</BtnMenu>
            <BtnMenu to='/activplayers'>Активные&nbsp;игроки</BtnMenu>
            <BtnMenu to='/storygames'>История&nbsp;игр</BtnMenu>
            <BtnMenu to='/allplayers'>Список&nbsp;игроков</BtnMenu>
        </div>
    );
};

export default NavBar;

