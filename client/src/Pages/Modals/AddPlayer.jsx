import React, { useState } from 'react';
import MainButton from '../../components/UI/buttons/MainButton';
import MainInput from '../../components/UI/input/MainInput';
import cl from './ModuleLogin.module.css';
import esc from '../../components/UI/XOmedia/Esc.svg'
import Girl from '../../components/UI/XOmedia/girl.svg'
import Man from '../../components/UI/XOmedia/man.svg'
import myAxi from '../../http/interceptor';
import ModalHeaders from '../../components/UI/Hats/ModalHeaders';


const AddPlayer = ({ visible, notVisible, Add }) => {

    let now = new Date();

    const [valueNewP, setValueNewP] = useState({ gender: true, age: '' });
    const [valNamePl, setValNamePl] = useState('');

    async function AddNewPl(e) {
        e.preventDefault();
        const NewPl = {
            statusActive: true,
            statusGame: false,
            createdAt: now.toISOString(),
            updatedAt: now.toISOString(),
            firstName: valNamePl.split(' ')[1],
            surName: valNamePl.split(' ')[0],
            secondName: valNamePl.split(' ')[2],
            ...valueNewP
        };
        await myAxi.post('/allplayers', NewPl);
        Add({...NewPl, id: Date.now()});
        setValueNewP({ gender: true, age: '' });
        setValNamePl('');
        notVisible(false);
    };

    const looking = [cl.fonMod];
    if (visible) {
        looking.push(cl.active)
    }

    return (
        <main className={looking.join(' ')}>
            <div className={cl.LoginWind}>
                <form>
                    <button onClick={e => notVisible(false, e.preventDefault())} className={cl.close}><img src={esc} alt='Закрыть' /></button>
                </form>

                <ModalHeaders>Добавьте игрока</ModalHeaders>

                <form style={{ gap: '8px' }} onSubmit={AddNewPl}>

                    <span className={cl.haters}>ФИО</span>

                    <MainInput type='text'
                        maxLength='255'
                        style={{ marginBottom: '12px' }}
                        value={valNamePl}
                        onChange={e => setValNamePl(e.target.value.replace(/[^A-za-zА-яа-я_-\s]/g, ''))}
                        placeholder='Иванов Иван Иванович' />

                    <span><pre className={cl.haters}>Возраст           Пол</pre></span>

                    <div className={cl.parameters}>

                        <MainInput type='text'
                            maxLength='3'
                            style={{ width: '72px', marginRight: '32px' }}
                            value={valueNewP.age}
                            onChange={e => setValueNewP({ ...valueNewP, age: e.target.value.replace(/[^\d]/g, '') })}
                            placeholder='0' />

                        <div className={cl.sexParam}>

                            <button className={valueNewP.gender ? [cl.choice, cl.Focus].join(' ') : cl.choice}
                                value={true}
                                onClick={e => { e.preventDefault(); setValueNewP({ ...valueNewP, gender: true }) }}>
                                <img src={Girl} alt='Ж' /></button>

                            <button className={valueNewP.gender ? cl.choice : [cl.choice, cl.Focus].join(' ')}
                                value={false}
                                onClick={e => { e.preventDefault(); setValueNewP({ ...valueNewP, gender: false }) }}>
                                <img src={Man} alt='М' /></button>

                        </div>

                    </div>

                    <MainButton style={{ marginTop: '12px' }}
                        disabled={(!valNamePl + !valueNewP.age)}>Добавить</MainButton>
                </form>
            </div>
        </main>
    );
};

export default AddPlayer;