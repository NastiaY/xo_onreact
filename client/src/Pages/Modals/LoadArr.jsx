import React from 'react';
import cl from './LoadArr.module.css';

const LoadArr = () => {

    return (
        <div className={cl.fonLoad}>
            <div className={cl.spin}/>
        </div>
    );
};

export default LoadArr;