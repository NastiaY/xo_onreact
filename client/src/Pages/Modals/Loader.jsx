import React from 'react';
import cl from './ModuleLogin.module.css';
import trofi from '../../components/UI/XOmedia/trofi.svg'

const Loader = () => {

    return (
        <div className={cl.fonanim}>
            <div><img src={trofi} alt='Трофей' className={cl.anim}/></div>
        </div>
    );
};

export default Loader;