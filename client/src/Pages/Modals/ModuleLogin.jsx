import React, { useState, useContext } from 'react';
import MainButton from '../../components/UI/buttons/MainButton';
import MainInput from '../../components/UI/input/MainInput';
import cl from './ModuleLogin.module.css';
import dog from '../../components/UI/XOmedia/dog.svg';
import { AuthContext } from '../../context/index';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import myAxi from '../../http/interceptor'
import Serv from '../../http/servService';
import ModalHeaders from '../../components/UI/Hats/ModalHeaders';


const ModuleLogin = () => {

    const { setIsUser, setIsAuth } = useContext(AuthContext);

    const router = useNavigate();

    const [valueLog, setValueLog] = useState("");
    const [valuePas, setValuePas] = useState("");
    const [valAlertP, setValAlertP] = useState('');
    const [valAlertL, setValAlertL] = useState('');

    const NotCopy = event => {
        event.preventDefault();
    }

    async function Login(event) {
        event.preventDefault();
        await axios.post('https://localhost:5000/api/login', { Login: valueLog, Pass: valuePas }, { withCredentials: true })
            .then(async(res) => {
                await Serv.getGame(res.data.id);
                // console.log(res.data);
                myAxi.get(`/rating/${res.data.id}`)
                    .then(result => {
                        setIsUser({ ...result.data[0], role: res.data.role });
                        setIsAuth(true);
                        router('/activplayers');
                    })
            })
            .catch(err => {
                if (err.response.status === 402) {
                    setValAlertP('Неверный пароль');
                    setIsAuth(false);
                } else if (err.response.status === 401) {
                    setValAlertL('Неверный логин');
                    setValAlertP('Неверный пароль');
                    setIsAuth(false);
                } else {
                    setValAlertP('Ваш аккаунт заблокирован');
                    setIsAuth(false);
                }
            })
    };


    return (
        <main className={cl.fonModLog}>
            <div className={cl.LoginWind}>

                <div><img src={dog} alt='Песик' /></div>

                <ModalHeaders>Войдите в игру</ModalHeaders>

                <form onSubmit={Login}>

                    <div>
                        <MainInput type='text' id='login'
                            maxLength='255'
                            value={valueLog}
                            onCopy={NotCopy}
                            onChange={event => setValueLog(event.target.value.replace(/[^\w\.]/g, ''))}
                            placeholder='Логин'
                            style={valAlertL ? { border: '1px solid #E93E3E' } : {}} />

                        {valAlertL ?
                            <span className={cl.notvalid}>Неверный логин</span> :
                            ''}
                    </div>

                    <div>
                        <MainInput type='password' id='pass'
                            maxLength='30'
                            value={valuePas}
                            onCopy={NotCopy}
                            onChange={event => setValuePas(event.target.value.replace(/[^\w\.]/g, ''))}
                            placeholder='Пароль'
                            style={valAlertP === 'Неверный пароль' ? { border: '1px solid #E93E3E' } : {}} />

                        {valAlertP === 'Неверный пароль' ?
                            <span className={cl.notvalid}>Неверный пароль</span> :
                            ''}

                        {valAlertP === 'Ваш аккаунт заблокирован' ?
                            <span className={cl.notvalid}>Ваш аккаунт заблокирован</span> :
                            ''}
                    </div>

                    <MainButton to='/raitings'
                        style={{ marginTop: '8px' }}
                        disabled={(!valueLog, !valuePas)}
                        type='submit'>Войти</MainButton>

                </form>

            </div>
        </main>
    );
};

export default ModuleLogin;