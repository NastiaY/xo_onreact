import React, { useState } from 'react';
import cl from './ModuleLogin.module.css';
import HeaderTables from '../../components/UI/Hats/HeaderTables';
import Omini from '../../components/UI/XOmedia/Omini.svg';
import Xmini from '../../components/UI/XOmedia/Xmini.svg';
import PlayersPlaing from '../../components/UI/Hats/PlayersPlaing';
import Game from '../GameField/GameClass';
import Up from '../../components/UI/XOmedia/up.svg';
import Down from '../../components/UI/XOmedia/down.svg';


const PlayersInGame = () => {

    const [upDown, setUpDown] = useState(false);

    return (
        <div className={cl.plaingNow}>

            {window.innerWidth <= 855 ?
                <HeaderTables style={{fontSize: '16px'}}>Игроки
                <img src={upDown ? Down : Up} alt='Развернуть' onClick={() => setUpDown(!upDown)}/>
                </HeaderTables> :
                <HeaderTables>Игроки</HeaderTables>}

            <div className={upDown ? cl.party : [cl.party, cl.notactive].join(' ')}>

                <div className={cl.krestNul}>
                    <div className={cl.iconO}><img src={Omini} alt='Нолики' /></div>
                    {Game.users.map((player) => {
                        if (player.id === Game.rolePl['o']) {
                            return <PlayersPlaing key={player.id} value={player} />
                        }
                    })}
                </div>

                <div className={cl.krestNul}>
                    <div className={cl.iconX}><img src={Xmini} alt='Крестики' /></div>
                    {Game.users.map((player) => {
                        if (player.id === Game.rolePl['x']) {
                            return <PlayersPlaing key={player.id} value={player} />
                        }
                    })}
                </div>

            </div>

        </div>
    );
};

export default PlayersInGame;