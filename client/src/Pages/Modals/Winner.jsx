import React, { useContext } from 'react';
import MainButton from '../../components/UI/buttons/MainButton';
import GreyBtn from '../../components/UI/buttons/GreyBtn';
import cl from './ModuleLogin.module.css';
import ModalHeaders from '../../components/UI/Hats/ModalHeaders';
import trofi from '../../components/UI/XOmedia/trofi.svg';
import { useNavigate } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import Game from '../GameField/GameClass';
import WSservice from '../../http/WebSocketService';
import { AuthContext } from '../../context/index';
import myAxi from '../../http/interceptor';


const Winner = () => {

    const { isUser } = useContext(AuthContext);

    const router = useNavigate();

    const BackMenu = (e) => {
        e.preventDefault();
        router('/activplayers');
    };

    const looking = [cl.fonMod];
    if (Game.isOver) {
        looking.push(cl.active);
    };

    async function moreGame(e) {
        e.preventDefault();
        await myAxi.get(`/allplayers?id=${Game.opponent}`)
            .then(res => {
                if (res.data[0].statusGame) {
                    WSservice.invitePlayer(Game.opponent, isUser);
                } else {
                    alert(`${res.data[0].firstName} уже в игре`)
                };
            });
    };


    return (
        <main className={looking.join(' ')}>

            <div className={cl.LoginWind}>

                <div><img src={trofi} alt="Трофей" /></div>

                {Game.isDraw ?
                    <ModalHeaders style={{ textAlign: "center" }}>Ничья!</ModalHeaders> :
                    <ModalHeaders style={{ textAlign: "center" }}>
                        {Game.users.map(pl => {
                            if (pl.id === Game.rolePl[Game.winner]) {
                                return pl.firstName + ' ' + pl.surName
                            }
                        })} победил!</ModalHeaders>
                }

                <div className={cl.winmod}>
                    <form><MainButton onClick={moreGame}>Новая игра</MainButton></form>

                    <form><GreyBtn onClick={BackMenu}>Выйти в меню</GreyBtn></form>
                </div>
            </div>
        </main>
    );
};

export default observer(Winner);