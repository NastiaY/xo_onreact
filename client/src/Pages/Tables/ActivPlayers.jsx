import React, { useState, useMemo, useEffect, useContext } from 'react';
import cl from './Tabl.module.css';
import HeaderTables from '../../components/UI/Hats/HeaderTables';
import BtnOnlyFree from '../../components/UI/buttons/BtnOnlyFree';
import AllActivPl from '../../components/AllActivPl';
import myAxi from '../../http/interceptor';
import { AuthContext } from '../../context/index';
import Game from '../GameField/GameClass';
import LoadArr from '../Modals/LoadArr';
import { observer } from 'mobx-react-lite';


const ActivPlayers = () => {

    const { isUser } = useContext(AuthContext);
    const [valueActPl, setValueActPl] = useState([]);
    const [isPlayerLoad, setPlayerLoad] = useState(true);

    useEffect(() => {
        myAxi.get('/active', { params: { id: isUser.id, players: Game.allUsersOnline } })
            .then((res) => {
                setValueActPl(res.data);
                setPlayerLoad(false);
            })
            .catch(err => {
                console.error(err);
                setPlayerLoad(false);
            });
    }, [Game.allUsersOnline]); // под вопросом

    const [SortPl, setSortActPl] = useState(false);

    const FilterPl = useMemo(() => {
        if (SortPl) {
            return (valueActPl.filter((pl) => pl.statusGame === SortPl));
        } else {
            return valueActPl;
        }
    }, [valueActPl, SortPl]);


    return (
        <main className={cl.fonPgs}>
            <div className={cl.fonTabl}>
                <div className={cl.hatActG}>

                    <HeaderTables>Активные игроки</HeaderTables>

                    <div style={{ display: 'flex', gap: '8px', lineHeight: '24px' }}>

                        <span>Только свободные</span>

                        <BtnOnlyFree value={SortPl} onChange={setSortActPl} />

                    </div>

                </div>

                <div className={cl.scrollActive}> 
                    {isPlayerLoad ?
                        <LoadArr /> :
                        <AllActivPl allplayers={FilterPl} />}
                </div>

            </div>
        </main>
    );
};

export default observer(ActivPlayers);