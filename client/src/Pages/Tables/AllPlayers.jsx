import React, { useEffect, useRef, useState } from 'react';
import GreenButton from '../../components/UI/buttons/GreenButton';
import classes from './Tabl.module.css';
import HeaderTables from '../../components/UI/Hats/HeaderTables';
import StrAllP from '../../components/UI/StrTabl/StrAllP';
import AddPlayer from '../Modals/AddPlayer';
import myAxi from '../../http/interceptor';
import cl from '../Modals/LoadArr.module.css';
import clas from '../../components/UI/StrTabl/StrTables.module.css';


const AllPlayers = () => {

    const [valueAllpl, setValuePl] = useState([]);
    const [Offset, setOffset] = useState(0);
    const [totalCount, setTotalCount] = useState(0);
    const [isPlayerLoad, setPlayerLoad] = useState(false);

    const LastElement = useRef();
    const observer = useRef();

    useEffect(() => {
        if (isPlayerLoad) return;
        if (observer.current) observer.current.disconnect();
        let callback = (entries, observer) => {
            if (entries[0].isIntersecting && Offset < totalCount) {
                setOffset(Offset + 20);
            }
        }
        observer.current = new IntersectionObserver(callback);
        observer.current.observe(LastElement.current);
    }, [isPlayerLoad])

    useEffect(() => {
        setPlayerLoad(true);
        myAxi.get('/allplayers', { params: { limit: 20, offset: Offset } })
            .then((res) => {
                setTotalCount(res.headers['x-total-count']);
                setValuePl([...valueAllpl, ...res.data]);
                setPlayerLoad(false);
            })
            .catch(err => {
                console.error(err);
                setPlayerLoad(false);
            });
    }, [Offset]);

    const [modal, setModal] = useState(false)

    const AddNewPlayer = (newPl) => {
        setValuePl([newPl, ...valueAllpl])
    };

    return (
        <main className={classes.fonPgs}>
            <AddPlayer visible={modal} notVisible={setModal} Add={AddNewPlayer} />
            <div className={classes.fonTabl}>
                <div className={classes.hatAllP}>
                    <HeaderTables>Список игроков</HeaderTables>
                    <GreenButton onClick={() => setModal(true)}>Добавить&nbsp;игрока</GreenButton>
                </div>
                <div>
                    <div className={classes.hatR}>
                        <span className={clas.nameR}>ФИО</span>
                        <span className={clas.ageAP}>Возраст</span>
                        <span className={clas.sexAP}>Пол</span>
                        <span className={clas.statAP}>Статус</span>
                        <span className={clas.creaAP}>Создан</span>
                        <span className={clas.creaAP }>Изменен</span>
                    </div>
                    <div className={classes.scrollTabl}>

                        {valueAllpl.map((players) => {
                            return <StrAllP key={players.id} info={players} />
                        })}

                        {Offset < totalCount ?
                            <div className={cl.fonLoad}>
                                <div ref={LastElement} className={cl.spin} />
                            </div> :
                            <div ref={LastElement} />}

                    </div>
                </div>
            </div>
        </main>
    );
};

export default AllPlayers;