import React, { useEffect, useRef, useState } from 'react';
import HeaderTables from '../../components/UI/Hats/HeaderTables';
import classes from './Tabl.module.css';
import StrRating from '../../components/UI/StrTabl/StrRating';
import myAxi from '../../http/interceptor';
import cl from '../Modals/LoadArr.module.css';
import clas from '../../components/UI/StrTabl/StrTables.module.css';


const Rating = () => {

    const [valuePl, setValPl] = useState([]);

    const [Offset, setOffset] = useState(0);
    const [totalCount, setTotalCount] = useState(0);
    const [isPlayerLoad, setPlayerLoad] = useState(false);

    const LastElement = useRef();
    const observer = useRef();

    useEffect(() => {
        if (isPlayerLoad) return;
        if (observer.current) observer.current.disconnect();
        let callback = (entries, observer) => {
            if (entries[0].isIntersecting && Offset < totalCount) {
                setOffset(Offset + 30);
            } 
        }
        observer.current = new IntersectionObserver(callback);
        observer.current.observe(LastElement.current);
    }, [isPlayerLoad])

    useEffect(() => {
        setPlayerLoad(true);
        myAxi.get('/rating', {params: {limit: 30, offset: Offset}})
            .then((res) => {
                setTotalCount(res.headers['x-total-count']);
                setValPl([...valuePl, ...res.data]);
                setPlayerLoad(false);
            })
            .catch(err => {
                console.error(err);
                setPlayerLoad(false);
            });        
    }, [Offset])

    return (
        <main className={classes.fonPgs}>
            <div className={classes.fonTabl}>
                <HeaderTables>Рейтинг игроков</HeaderTables>
                <div>
                    <div className={classes.hatR}>
                        <span className={clas.nameR }>ФИО</span>
                        <span className={clas.allGR}>Всего&nbsp;игр</span>
                        <span className={clas.hatWin}>Победы</span>
                        <span className={clas.hatLos}>Проигрыши</span>
                        <span className={clas.procR}>Процент&nbsp;побед</span>
                    </div>
                    <div className={classes.scrollRating}>

                        {valuePl.map((players) => {
                                return <StrRating key={players.id} info={players} />
                            })}

                        {Offset < totalCount ?
                        <div className={cl.fonLoad}>
                            <div ref={LastElement} className={cl.spin}/>
                        </div> :
                        <div ref={LastElement}/>}

                    </div>
                </div>
            </div>
        </main>
    );
};

export default Rating;
