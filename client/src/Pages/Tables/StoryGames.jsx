import React, { useContext, useEffect, useState } from 'react';
import HeaderTables from '../../components/UI/Hats/HeaderTables';
import cl from './Tabl.module.css';
import StrStory from '../../components/UI/StrTabl/StrStory';
import myAxi from '../../http/interceptor';
import LoadArr from '../Modals/LoadArr';
import { AuthContext } from '../../context/index';

const StoryGames = () => {

    const {isUser} = useContext(AuthContext);
    const [valueGames, setValGames] = useState([]);
    const [isPlayerLoad, setPlayerLoad] = useState(true);

    useEffect(() => {
        myAxi.get(`/allgames`)
            .then((res) => {
                setValGames(res.data);
                setPlayerLoad(false);
            })
            .catch(err => {
                console.error(err);
                setPlayerLoad(false);
            });                
    }, []);

    return (
        <main className={cl.fonPgs}>
            <div className={cl.fonTabl}>
                <HeaderTables>История игр</HeaderTables>
                <div>
                    <div className={cl.hatSt}>
                        <span className={cl.hatStoryPl}>Игроки</span>
                        <span className={cl.hatStoryD}>Дата</span>
                        <span className={cl.hatStoryT}>Время игры</span>
                    </div>
                    <div className={cl.tablStG}>
                        {isPlayerLoad ?
                        <LoadArr/> :
                        valueGames.map((gm) => {
                            return <StrStory key={gm.id} info={gm}/>
                        })}

                    </div>
                </div>
            </div>
        </main>
    );
};

export default StoryGames;
