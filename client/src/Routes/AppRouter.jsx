import React, { useContext } from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { privateRoutes, publicRoutes, adminRouters } from './index';
import Header from '../Pages/Header/Header';
import { AuthContext } from '../context/index';
import Loader from '../Pages/Modals/Loader';


const AppRouter = () => {

    const { isUser, isAuth, isLoad } = useContext(AuthContext);

    if (isLoad) {
        return <Loader />;
    };

    return (
        isAuth
            ? (
                <BrowserRouter>
                    <Header />
                    <Routes>
                        {privateRoutes.map(route =>
                            <Route
                                element={route.component}
                                path={route.path}
                                exact={route.exact}
                                key={route.path} />
                        )}
                        <Route path='/*' element={<Navigate to='/ratings' replace />} />
                        {isUser.role ?
                            adminRouters.map(route =>
                                <Route
                                    element={route.component}
                                    path={route.path}
                                    exact={route.exact}
                                    key={route.path} />
                            ) :
                            null}
                    </Routes >
                </BrowserRouter>
            ) : (
                <BrowserRouter>
                    <Routes>
                        {publicRoutes.map(route =>
                            <Route
                                element={route.component}
                                path={route.path}
                                exact={route.exact}
                                key={route.path} />
                        )}
                        <Route path='/*' element={<Navigate to='/' replace />} />
                    </Routes >
                </BrowserRouter>
            )
    );
};

export default AppRouter;