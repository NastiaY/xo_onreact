import Rating from '../Pages/Tables/Rating';
import PlaingField from '../Pages/GameField/PlaingField';
import ModuleLogin from '../Pages/Modals/ModuleLogin';
import ActivPlayers from '../Pages/Tables/ActivPlayers';
import StoryGames from '../Pages/Tables/StoryGames';
import AllPlayers from '../Pages/Tables/AllPlayers';

export const privateRoutes = [
    {path: '/plaingfield/:id', component: <PlaingField/>, exact: true},
    {path: '/plaingfield', component: <PlaingField/>, exact: true},
    {path: '/ratings', component: <Rating/>, exact: true},
    {path: '/activplayers', component: <ActivPlayers/>, exact: true},
    {path: '/storygames', component: <StoryGames/>, exact: true},
]

export const publicRoutes = [
    {path: '/', component: <ModuleLogin/>, exact: true},
]

export const adminRouters = [
    {path: '/allplayers', component: <AllPlayers/>, exact: true}
]