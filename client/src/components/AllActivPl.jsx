import React from 'react';
import StrActG from './UI/StrTabl/StrActG';
import cl from './Chat.module.css';

const AllActivPl = ({ allplayers }) => {

    return (
        <div className={cl.onlinePl}>
            {allplayers.map((players) => {
                return <StrActG key={players.id} info={players} />
            })}
        </div>
    );
};

export default AllActivPl;