import React, { useContext, useState } from 'react';
import cl from './Chat.module.css';
import PostList from './PostList';
import MainInput from './UI/input/MainInput';
import MainButton from './UI/buttons/MainButton';
import send from './UI/XOmedia/send.svg';
import { observer } from 'mobx-react-lite';
import Game from '../Pages/GameField/GameClass';
import { AuthContext } from '../context/index';
import Chat from '../Pages/GameField/GameChat';
import Serv from '../http/servService';
import WSservice from '../http/WebSocketService';


const ChatForm = () => {

    const [inpValue, setInpVal] = useState('');
    const { isUser } = useContext(AuthContext);    

    var now = new Date();

    async function AddNewMsg(e) {
        e.preventDefault();
        const message = inpValue;
        const newMsg = {
            createdAt: now.toISOString(),
            idGame: Game.session,
            idUser: isUser.id,
            message: message
        };
        WSservice.socket.send(JSON.stringify({...newMsg, method: 'chat', id: Game.session}));
        setInpVal('');
        Serv.postChat(newMsg);
    };
    
    return (
        <div className={cl.chat}>

            <div className={cl.fixChat}>
                <PostList message={Chat.messages} />
            </div>

            <div className={cl.chatInput}>
                <form style={{ display: 'flex', flexDirection: 'row' }}>

                    <MainInput type='text'
                        style={window.innerWidth > 855 ? { width: '352px' } : { width: '306px' } }
                        maxLength='500'
                        value={inpValue}
                        onChange={e => setInpVal(e.target.value)} placeholder='Сообщение' />

                    <MainButton
                        disabled={inpValue === ''}
                        style={{ width: '48px' }}
                        onClick={AddNewMsg}><img src={send} alt='отправить' /></MainButton>
                </form>
            </div>

        </div>
    );
};

export default observer(ChatForm);