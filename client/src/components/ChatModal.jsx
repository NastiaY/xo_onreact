import React, { useContext } from 'react';
import cl from './Chat.module.css';
import { AuthContext } from '../context';
import Game from '../Pages/GameField/GameClass';


const ChatModal = (props) => {

    const { isUser } = useContext(AuthContext);

    const time = new Date(props.msg.createdAt);

    const user = (props.msg.idUser === isUser.id) ? Game.users[0] : Game.users[1]; 
    
    return (
        <div className={props.msg.idUser === isUser.id ? cl.me : cl.other}>

            <div className={cl.hatChat}>
                <span className={props.msg.idUser === Game.rolePl['o'] ? cl.authorO : cl.authorX}>
                    {user.surName} {user.firstName}
                </span>
                
                <span className={cl.time}>
                    {time.getHours()}:{String(time.getMinutes()).padStart(2, '0')}
                </span>
            </div>

            <span className={cl.msg}>{props.msg.message}</span>

        </div>
    );
};

export default ChatModal;