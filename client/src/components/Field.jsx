import React, { useContext } from 'react';
import cl from './Chat.module.css';
import Cell from './UI/buttons/Cell';
import { observer } from 'mobx-react-lite';
import Game from '../Pages/GameField/GameClass';
import { AuthContext } from '../context/index';
import Serv from '../http/servService';


const Field = (props) => {

    const { isUser } = useContext(AuthContext);

    async function Move(index, e) {
        e.preventDefault();
        if (Game.rolePl[Game.mode] === isUser.id) { 
            await Game.makeMove(index);
            if (Game.isOver) {
                await Serv.updateGame(Date.now());
            };
            await Serv.updateField(Game.session, Game.field); 
        } else {
            console.log('Дождитесь своего хода');
        }
    };

    return (
        <div className={cl.gameBoard}>

            {Game.field.map((cell, index) => {
                return <Cell {...props}
                    id={index} key={index}
                    onClick={e => Move(index, e)}>{cell}</Cell>
            })}

        </div>
    );
};

export default observer(Field);