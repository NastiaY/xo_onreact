import React from 'react';
import cl from './Chat.module.css';
import ChatModal from './ChatModal';


const PostList = ({ message }) => {

    return (
        <div>
            <div className={cl.posts}>
                {(message.map((post) => {
                    return <ChatModal msg={post} key={post.createdAt} />
                }))}

            </div>
        </div>
    );
};

export default PostList;