import React from 'react';
import cl from './ModalHeaders.module.css';

const HeaderTables = ({children, ...props}) => {

    return (
        <span {...props} className={cl.headerTable}>{children}</span> 
    );
};

export default HeaderTables;