import React from 'react';
import cl from './ModalHeaders.module.css';

const ModalHeaders = ({children, ...props}) => {

    return (
        <span {...props} className={cl.come}>{children}</span> 
    );
};

export default ModalHeaders;