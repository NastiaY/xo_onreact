import React from 'react';
import cl from './ModalHeaders.module.css';

const PlayersPlaing = (props) => {

    return (
        <div className={cl.dataPlayer}>
            <span className={cl.gamers}>
                {props.value.surName + ' ' + props.value.firstName + ' ' + props.value.secondName}</span> 
            <span className={cl.info}>{Math.round(props.value.wins_prc)}% побед</span>
        </div>
    );
};

export default PlayersPlaing;