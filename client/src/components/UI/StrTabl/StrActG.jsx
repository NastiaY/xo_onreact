import React, { useContext } from 'react';
import cl from './StrTables.module.css';
import classes from '../buttons/MainButton.module.css';
import ReadyBtn from '../buttons/ReadyBtn';
import InGame from '../buttons/InGame';
import GreenButton from '../buttons/GreenButton';
import WSservice from '../../../http/WebSocketService';
import { AuthContext } from '../../../context/index';
import myAxi from '../../../http/interceptor';
import Game from '../../../Pages/GameField/GameClass';


const StrActG = (props) => {

    const { isUser } = useContext(AuthContext);

    async function invitePl(e) {
        e.preventDefault();
        await myAxi.get(`/allplayers?id=${props.info.id}`)
            .then(res => {
                if (res.data[0].statusGame) {
                    WSservice.invitePlayer(props.info.id, isUser);
                } else {
                    alert(`${props.info.firstName} уже в игре`)
                };
            })
    };

    return (
        <div className={cl.strActG}>

            <span className={cl.nameActG}>
                {props.info.surName + ' ' + props.info.firstName + ' ' + props.info.secondName}</span>

            <div className={cl.btnsActG}>

                {props.info.statusGame ?
                    <ReadyBtn>Свободен</ReadyBtn> :
                    <InGame>В&nbsp;игре</InGame>}

                {props.info.statusGame ?
                    <form onClick={e => invitePl(e)}>
                        <GreenButton disabled={Game.session ? !Game.isOver : false}>Позвать&nbsp;играть</GreenButton>
                    </form> :
                    <button disabled className={classes.greyActPl}>Позвать играть</button>}
            </div>
        </div>
    );
};

export default StrActG;