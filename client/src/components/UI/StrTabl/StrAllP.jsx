import React, { useState } from 'react';
import cl from './StrTables.module.css';
import Blocked from '../buttons/Blocked';
import AddBlock from '../buttons/AddBlock';
import blockPl from '../XOmedia/blockPl.svg';
import girl from '../XOmedia/girl.svg';
import man from '../XOmedia/man.svg';
import ReadyBtn from '../buttons/ReadyBtn';
import myAxi from '../../../http/interceptor';
import moment from 'moment';
import 'moment/locale/ru';


const StrAllP = ({ info }) => {

    const [Player, setPlayer] = useState(info);

    let now = new Date();

    async function changedStatus(e, newStatus) {
        e.preventDefault()
        setPlayer({ ...Player, statusActive: newStatus, updatedAt: now.toISOString() });
        const upParams = { statusActive: newStatus, updatedAt: now.toISOString() }
        await myAxi.put(`/allplayers?id=${info.id}`, upParams);
        
    };

    return (
        <div className={cl.strAllP}>

            <span className={cl.nameR}>
                {info.surName + ' ' + info.firstName + ' ' + info.secondName}</span>

            <span className={cl.ageAP}>{info.age}</span>

            <div className={cl.sexAP}>
                {info.gender ?
                    <img src={girl} alt='Ж' /> :
                    <img src={man} alt='М' />}</div>

            <div className={cl.statAP}>
                {Player.statusActive ?
                    <ReadyBtn style={{ width: '136px' }}>Активен</ReadyBtn> :
                    <Blocked>Заблокирован</Blocked>}</div>

            <span className={cl.creaAP}>
                {moment(Player.createdAt).locale('ru').format('D MMMM YYYY')}</span>

            <span className={cl.creaAP}>
                {moment(Player.updatedAt).locale('ru').format('D MMMM YYYY')}</span>

            <div className={cl.blockAP}>
                {Player.statusActive ?
                    <AddBlock onClick={e => changedStatus(e, false)}>
                        <img src={blockPl} alt='Заблокировать' />Заблокировать</AddBlock> :
                    <AddBlock onClick={e => changedStatus(e, true)}>Разблокировать</AddBlock>}</div>

        </div>
    );
};

export default StrAllP;