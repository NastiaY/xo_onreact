import React from 'react';
import cl from './StrTables.module.css';

const StrTabl = (props) => {

    return (
        <div className={cl.strR}>
            <span className={cl.nameR}>
                {props.info.firstName + ' ' + props.info.surName + ' ' + props.info.secondName}
                </span>
            <span className={cl.allGR}>{props.info.total}</span>
            <span className={cl.winsR}>{props.info.wins}</span>
            <span className={cl.failR}>{props.info.loses}</span>
            <span className={cl.procR}>
                {Math.round(props.info.wins_prc)} %
                </span>
        </div>
    );
};

export default StrTabl;