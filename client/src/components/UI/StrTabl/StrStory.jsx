import React from 'react';
import cl from './StrTables.module.css';
import cubok from '../XOmedia/cubok.svg';
import Omini from '../XOmedia/Omini.svg';
import Xmini from '../XOmedia/Xmini.svg';
import moment from 'moment';
import 'moment/locale/ru';


const StrStory = (props) => {

    const startG = new Date(props.info.start);
    const endG = new Date(props.info.end);
    const time = endG - startG;

    return (
        <div className={cl.strSt}>
            <div className={cl.namesSt}>

                <span className={cl.nameOSt}>
                    <img src={Omini} alt='Нолики' style={{ width: '20px' }} />
                    {props.info.plO_surName + ' ' + props.info.plO[0] + '. ' + props.info.plO_secondName[0] + '.'}
                    {props.info.gameResult ?
                        <img src={cubok} alt='Победитель' className={cl.cubokPh} /> :
                        ''}
                </span>

                <span className={cl.contr}>против</span>

                <span className={cl.nameXSt}>
                    <img src={Xmini} alt='Крестики' style={{ width: '16px' }} />
                    {props.info.plX_surName + ' ' + props.info.plX[0] + '. ' + props.info.plX_secondName[0] + '.'}
                    {props.info.gameResult === false ?
                        <img src={cubok} alt='Победитель' className={cl.cubokPh} /> :
                        ''}
                </span>

            </div>

            <span className={cl.dateSt}>
                {moment(props.info.start).locale('ru').format('D MMMM YYYY')}</span>

            <span className={cl.timeSt}>
                {String(Math.floor(time / 1000 / 60)).padStart(2, 0) + ' мин ' + String(Math.ceil(time / 1000 % 60)).padStart(2, 0) + ' сек'}
            </span>

        </div>
    );
};

export default StrStory;