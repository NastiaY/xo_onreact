import React from 'react';
import cl from './MainButton.module.css';

const AddBlock = ({children, ...props}) => {

    return (
        <form>
            <button {...props} className={cl.addblock}>{children}</button>
        </form>
    );
};

export default AddBlock;