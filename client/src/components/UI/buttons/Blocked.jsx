import React from 'react';
import cl from './MainButton.module.css';

const Blocked = ({children, ...props}) => {

    return (
        <div {...props} className={cl.block}>{children}</div>
    );
};

export default Blocked;