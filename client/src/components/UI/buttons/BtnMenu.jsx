import React from 'react';
import cl from './MainButton.module.css';
import { NavLink } from 'react-router-dom'


const BtnMenu = ({ children, ...props }) => {

    return (
        <NavLink {...props}
            className={({ isActive }) => isActive ? cl.hoverActive : cl.hover}>{children}</NavLink>
    );
};

export default BtnMenu;