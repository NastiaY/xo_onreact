import React from 'react';
import cl from './MainButton.module.css';

const BtnOnlyFree = ({ value, onChange }) => {

    return (
        <div style={{ alignItems: "center", display: "flex" }}>
            <label className={cl.onlyFree}>
                <input type="checkbox" 
                onChange={e => onChange(value ? false : true)} />
                <div className={cl.ball}></div>
            </label>
        </div>
    );
};

export default BtnOnlyFree;