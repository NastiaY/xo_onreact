import React from 'react';
import cl from './MainButton.module.css';
import XXX from '../XOmedia/XXX.svg';
import OOO from '../XOmedia/OOO.svg';
import Game from '../../../Pages/GameField/GameClass';

const Cell = ({ children, ...props }) => {

    const colorCell = { 'x': cl.colorX, 'o': cl.colorO }

    const rend = (value) => {
        if (value === 'x') return <img src={XXX} alt='Крестик' />;
        else if (value === 'o') return <img src={OOO} alt='Нолик' />;
        else return;
    };

    const clCell = [cl.cell];
    if (Game.winComb.includes(props.id)) clCell.push(colorCell[children]);

    return (
        <form>
            <button {...props} className={clCell.join(' ')}>{rend(children)}</button>
        </form>
    );
};

export default Cell;