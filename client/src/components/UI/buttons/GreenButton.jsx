import React from 'react';
import cl from './MainButton.module.css';

const GreenButton = ({ children, ...props }) => {

    return (
        <button {...props} className={cl.greenBtn}>{children}</button>
    );
};

export default GreenButton;