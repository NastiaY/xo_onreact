import React from 'react';
import cl from './MainButton.module.css';

const GreyBtn = ({ children, ...props }) => {

    return (
        <button {...props} className={cl.grey}>{children}</button>
    );
};

export default GreyBtn;