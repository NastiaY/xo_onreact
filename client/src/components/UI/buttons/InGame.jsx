import React from 'react';
import cl from './MainButton.module.css';

const InGame = ({children, ...props}) => {

    return (
        <div {...props} className={cl.busy}>{children}</div>
    );
};

export default InGame;