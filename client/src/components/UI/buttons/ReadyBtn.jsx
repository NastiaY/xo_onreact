import React from 'react';
import cl from './MainButton.module.css';

const ReadyBtn = ({children, ...props}) => {

    return (
        <div {...props} className={cl.ready}>{children}</div>
    );
};

export default ReadyBtn;