import { makeAutoObservable } from 'mobx';
import { makePersistable } from 'mobx-persist-store';
import Game from '../../../Pages/GameField/GameClass';


class TimerField {

    startGame = 0;
    endGame = 0;
    timerX = 60000;
    timerO = 60000;
    _job;

    constructor() {
        makeAutoObservable(this);

        makePersistable(this, {
            name: 'TimeStore',
            properties: ['timerX', 'timerO',
                'startGame', 'endGame'],
            storage: window.localStorage,
            expireIn: 86400000 * 30,
        })
    };

    setStartGame(date) {
        this.startGame = date;
    };

    setEndGame(date) {
        this.endGame = date;
    };

    newTimer() {
        this.startGame = 0;
        this.endGame = 0;
        this.timerX = 60000;
        this.timerO = 60000;
        this.startTimer();
    };

    startTimer() {
        if (this._job) {
            clearInterval(this._job);
        };
        const callback = (Game.mode === 'x') ? this.setXtime : this.setOtime;
        this._job = setInterval(() => callback(), 1000);
    };

    setXtime = () => {
            this.timerX = this.timerX - 1000;   
    };

    setOtime = () => {
            this.timerO = this.timerO - 1000;  
    };
};

export default new TimerField();