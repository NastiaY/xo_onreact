import { observer} from 'mobx-react-lite';
import React, { useContext, useRef } from 'react';
import Timer from './TimerField';
import cl from './Timer.module.css';
import Game from '../../../Pages/GameField/GameClass';
import { AuthContext } from '../../../context';


const TimerGame = () => {

    const {isUser} = useContext(AuthContext);
    const strSec = useRef();
    const strMin = useRef();

    if (Game.rolePl['o'] === isUser.id) {
        strSec.current = String(Math.floor((Timer.timerO / 1000) % 60));
        strMin.current = String(Math.floor((Timer.timerO / 1000 / 60) % 60));
    } else {
        strSec.current = String(Math.floor((Timer.timerX / 1000) % 60));
        strMin.current = String(Math.floor((Timer.timerX / 1000 / 60) % 60));
    }
    

    return (
        <div className={cl.timer}>
            
            {strMin.current.padStart(2, '0')}:{strSec.current.padStart(2, '0')}</div>
    );
};

export default observer(TimerGame);