import Game from '../Pages/GameField/GameClass';
import Chat from '../Pages/GameField/GameChat';
import Timer from '../components/UI/timer/TimerField';
import Serv from './servService';


class WebSocketService {
    constructor() {
        this.socket = null;
    };

    connect() {
        this.socket = new WebSocket('wss://localhost:5000/');
    };

    disconnect(id) {
        if (this.socket) {
            this.socket.send(JSON.stringify({
                method: 'disconnect',
                userId: id
            }));
            this.socket.close();
            this.socket = null;
        };
    };

    handleOpen(user) {
        this.socket.send(JSON.stringify({
            method: 'connect',
            userId: user.id,
            message: `Игрок ${user.firstName} ${user.surName} подключился`
        }));
    };

    handleOpenGame(idSession) {
        this.socket.send(JSON.stringify({
            method: 'gameConnect',
            id: idSession,
        }));
    };

    invitePlayer(idOpponent, isUser) {
        this.socket.send(JSON.stringify({
            method: 'invite',
            userId: idOpponent,
            inviter: isUser.id,
            name: isUser.firstName + ' ' + isUser.surName
        }));
    };

    async CaseInvite(msg) {
        Game.newGame();
        Chat.newChat();
        Timer.newTimer();
        Game.setOpponent(msg.inviter);
        Game.setRolepl(this.getRolePlay(msg.userId, msg.inviter));
        Timer.setStartGame(Date.now());
        await Serv.postNewGame();
        this.socket.send(JSON.stringify({
            method: 'answer',
            gameSes: Game.session,
            userId: msg.inviter,
            roles: Game.rolePl,
            opponent: msg.userId,
        }));
    };

    CaseAnswer(msg) {
        Game.newGame();
        Chat.newChat();
        Timer.newTimer();
        Timer.setStartGame(Date.now());
        Game.setSessionId(msg.gameSes);
        Game.setOpponent(msg.opponent)
        Game.setRolepl(msg.roles);
    };

    async CaseNo(msg, user) {
        this.socket.send(JSON.stringify({
            method: 'no',
            userId: msg.inviter,
            message: `Игрок ${user.firstName} отклонил приглашение`
        }));
    };

    async CaseGiveUp(msg) {
        this.socket.send(JSON.stringify({
            field: msg,
            method: 'giveup',
            id: Game.session
        }));
    };

    async CaseGame(ind, mode, ses) {
        this.socket.send(JSON.stringify({
            fieldIndex: ind,
            mode: mode,
            method: 'game',
            id: ses
        }))
    };

    getRolePlay = (userid, inviterid) => {
        const roles = ['x', 'o'];
        let newRoles = {};
        const randRole = roles[Math.floor(Math.random() * 2)];
        const secondRole = (randRole === 'x') ? 'o' : 'x';
        newRoles[randRole] = userid;
        newRoles[secondRole] = inviterid;
        return newRoles;
    };
};

export default new WebSocketService();