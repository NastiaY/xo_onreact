import axios from "axios";


export const API_URL = 'https://localhost:5000/api/';

const intAxi = axios.create({
    withCredentials: true,
    baseURL: API_URL,
});

intAxi.interceptors.request.use((config) => {
    return config;
});

intAxi.interceptors.response.use((config) => {
    return config;
}, async (error) => {
    const origRequest = error.config;
    if (error.response.status == 401 && error.config && !error.config._isRetry) {
        origRequest._isRetry = true;
        try {
            await axios.get(`${API_URL}/order`, { withCredentials: true})
            return intAxi.request(origRequest);
        } catch(err) {
            console.log('Вы не авторизованы')
        }
    }
});

export default intAxi;