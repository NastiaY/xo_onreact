import myAxi from './interceptor';
import Game from '../Pages/GameField/GameClass';
import Chat from '../Pages/GameField/GameChat';
import Timer from '../components/UI/timer/TimerField';


class ServService {

    async postNewGame() {
        let time = new Date(Timer.startGame);
        let data = {
            idPlayerO: Game.rolePl['o'],
            idPlayerX: Game.rolePl['x'],
            gameBegin: time.toISOString(),
            gameResult: null,
            gameEnd: null
        };
        await myAxi.post('/games', data)
            .then(res => {
                Game.setSessionId(res.data[0].id)
            })
            .catch(err => {
                console.error(err);
            })
    };

    async getGame(id) {
        try {
            await myAxi.get(`/games?id=${id}`)
                .then(res => {
                    if (res.data) {
                        Game.setField(res.data.field);
                        Game.setSessionId(res.data.id);
                        Game.setRolepl({ 'o': res.data.idPlayerO, 'x': res.data.idPlayerX });
                        let start = new Date(res.data.gameBegin);
                        Timer.setStartGame(start.getTime());
                        if (res.data.gameEnd) {
                            let end = new Date(res.data.gameEnd);
                            Timer.setEndGame(end.getTime());
                        };
                        let opp = (id === res.data.idPlayerO) ? res.data.idPlayerX : res.data.idPlayerO;
                        Game.setOpponent(opp);
                    } else {
                        Game.newGame();
                    }
                })
        } catch (err) {
            console.error(err)
        };
    };

    async updateGame(now) {
        const varRes = { 'o': true, 'x': false, null: null };
        let Res = varRes[Game.winner];
        let time = new Date(now);
        await myAxi.put(`/games?id=${Game.session}`, { gameEnd: time.toISOString(), gameResult: Res })
            .then(res => {
                console.log(res.data);
            })
            .catch(err => console.error(err));
    };

    async updatePlayer(id, stat) {
        await myAxi.put(`/allplayers?id=${id}`, { statusGame: stat })
            .then(res => {
                // console.log(res.data);
            })
            .catch(err => console.error(err));
    };

    async updateField(id, field) {
        await myAxi.put(`/field?id=${id}`, { field })
            .then(res => {
                // console.log(res);
            })
            .catch(err => console.error(err));
    };

    async getField(id) {
        await myAxi.get(`/field?id=${id}`)
            .then(res => {
                Game.setField(res.data);
            })
    }

    async getChat(ind) {
        await myAxi.get(`/chat?id=${ind}`)
            .then(res => {
                Chat.setMessages(res.data);
            })
            .catch(err => console.error(err));
    };

    async postChat(data) {
        await myAxi.post(`/chat`, data)
            .then(res => {
                // console.log(res.data)
            })
            .catch(err => console.error(err));
    };
};

export default new ServService();