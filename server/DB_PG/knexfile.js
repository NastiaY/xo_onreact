require('dotenv').config();

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {

    development: {
        client: 'pg',
        connection:
            JSON.parse(process.env.PG_CONNECTION),
        // debug: true
    },
    migrations: {
        directory: 'migrations',
        tableName: 'knex_migrations',
    },
    seeds: {
        directory: 'seeds',
    }

};
