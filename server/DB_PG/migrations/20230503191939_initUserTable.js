/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
    .createTable('users', function (table) {
        table.increments('id').primary();
        table.string('firstName', 255).notNullable();
        table.string('surName', 255).notNullable();
        table.string('secondName', 255).notNullable();
        table.integer('age').unsigned().notNullable();
        table.boolean('gender').notNullable();
        table.boolean('statusActive').notNullable();
        table.boolean('statusGame').notNullable();
        table.timestamp('createdAt').notNullable();
        table.timestamp('updatedAt').notNullable();
    })
    .createTable('accounts', function (table) {
        table.integer('idUser').unique().notNullable();
        table.foreign('idUser').references('users.id');
        table.string('login', 255).unique().notNullable();
        table.string('password', 255).notNullable();
    })
    .createTable('games', function (table) {
        table.increments('id').primary();
        table.integer('idPlayerO').notNullable();
        table.foreign('idPlayerO').references('users.id');
        table.integer('idPlayerX').notNullable();
        table.foreign('idPlayerX').references('users.id');
        table.boolean('gameResult');
        table.timestamp('gameBegin').notNullable();
        table.timestamp('gameEnd');
    })
    .createTable('messenger', function (table) {
        table.integer('idGame').notNullable();
        table.foreign('idGame').references('games.id');
        table.integer('idUser').notNullable();
        table.foreign('idUser').references('users.id');
        table.string('message').notNullable();
        table.timestamp('createdAt').notNullable();
    })

};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema
    .dropTable('accounts')
    .dropTable('messenger')
    .dropTable('games')
    .dropTable('users')

};
