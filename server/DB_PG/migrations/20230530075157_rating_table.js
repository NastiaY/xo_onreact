/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
    await knex.schema
        .createTable('counters', function (table) {
            table.increments('id').primary();
            table.integer('user_id').notNullable();
            table.foreign('user_id').references('users.id');
            table.specificType('symbol', 'char(1)').notNullable();
            table.timestamp('period').notNullable().defaultTo(knex.raw(`date_trunc('month', now())`));
            table.integer('wins').notNullable().defaultTo(0);
            table.integer('loses').notNullable().defaultTo(0);
            table.integer('draws').notNullable().defaultTo(0);
            table.unique(['user_id', 'symbol', 'period']);
        })

    const resultO = await knex('users').select('users.id as user_id',
        knex.raw(`'o' as symbol`),
        knex.raw(`date_trunc('month', games."gameBegin") as period`),
        knex.raw(`COUNT(games.id) FILTER (WHERE games."gameResult" = TRUE) AS wins`),
        knex.raw(`COUNT(games.id) FILTER (WHERE games."gameResult" = FALSE) AS loses`),
        knex.raw(`COUNT(games.id) FILTER (WHERE games."gameResult" IS NULL) AS draws`)
        )
        .join('games', function () { this.on('users.id', '=', 'games.idPlayerO') })
        .groupByRaw(`users.id, date_trunc('month', games."gameBegin")`);
    const resultX = await knex.select('users.id as user_id',
        knex.raw(`'x' as symbol`),
        knex.raw(`date_trunc('month', games."gameBegin") as period`),
        knex.raw(`COUNT(games.id) FILTER (WHERE games."gameResult" = FALSE) AS wins`),
        knex.raw(`COUNT(games.id) FILTER (WHERE games."gameResult" = TRUE) AS loses`),
        knex.raw(`COUNT(games.id) FILTER (WHERE games."gameResult" IS NULL) AS draws`)
        )
        .from('users')
        .join('games', function () { this.on('users.id', '=', 'games.idPlayerX') })
        .groupByRaw(`users.id, date_trunc('month', games."gameBegin")`);
    for (let i = 0; i < resultO.length; i++) {
        await knex('counters').insert(resultO[i]);
    }
    for (let i = 0; i < resultX.length; i++) {
        await knex('counters').insert(resultX[i]);
    }
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    return knex.schema.dropTable('counters');
};
