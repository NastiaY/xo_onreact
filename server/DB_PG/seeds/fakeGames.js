/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
const { faker } = require('@faker-js/faker');

exports.seed = async function(knex) {

  const genGamesO = (count, start, end, minId, maxId ) => {
    let games = [];
    for (let i = 0; i < count; i++) {
      let endGame = faker.date.between({from: start, to: end});
      let fromStart = new Date(endGame.getTime() - (3600 * 1000));
      let toEnd = new Date(endGame.getTime() - (10 * 1000));
      let startGame = faker.date.between({from: fromStart, to: toEnd});
      games.push({  
        idPlayerO: faker.number.int({ min: minId, max: maxId }),
        idPlayerX: faker.number.int({ min: minId, max: 2018 }),
        gameResult: faker.helpers.arrayElement([true, false, null]), 
        gameBegin: startGame,
        gameEnd: endGame
      })
    }
    return games;
  };
  
  const genGamesX = (count, start, end, minId, maxId ) => {
    let games = [];
    for (let i = 0; i < count; i++) {
      let endGame = faker.date.between({from: start, to: end});
      let fromStart = new Date(endGame.getTime() - (3600 * 1000));
      let toEnd = new Date(endGame.getTime() - (10 * 1000));
      let startGame = faker.date.between({from: fromStart, to: toEnd});
      games.push({  
        idPlayerO: faker.number.int({ min: minId, max: 2018 }),
        idPlayerX: faker.number.int({ min: minId, max: maxId }),
        gameResult: faker.helpers.arrayElement([true, false, null]), 
        gameBegin: startGame,
        gameEnd: endGame
      })
    }
    return games;
  };

  for (let j = 0; j < 10; j++) {
    let gamesArrO = genGamesO(10000, '2022-05-01T01:01:00.000Z', '2022-10-31T00:00:00.000Z', 18, 217);
    let gamesArrX = genGamesX(10000, '2022-05-01T01:01:00.000Z', '2022-10-31T00:00:00.000Z', 18, 217);
    await knex('games').insert(gamesArrO);
    await knex('games').insert(gamesArrX);
  }
  for (let j = 0; j < 10; j++) {
    let gamesArrO= genGamesO(10000, '2022-05-01T01:01:00.000Z', '2023-01-31T00:00:00.000Z', 218, 417);
    let gamesArrX = genGamesX(10000, '2022-05-01T01:01:00.000Z', '2023-01-31T00:00:00.000Z', 218, 417);
    await knex('games').insert(gamesArrO);
    await knex('games').insert(gamesArrX);
  }
  for (let j = 0; j < 3; j++) {
    let gamesArrO = genGamesO(10000, '2023-03-01T02:01:00.000Z', '2023-05-25T00:00:00.000Z', 218, 417);
    let gamesArrX = genGamesX(10000, '2023-03-01T02:01:00.000Z', '2023-05-25T00:00:00.000Z', 218, 417);
    await knex('games').insert(gamesArrO);
    await knex('games').insert(gamesArrX);
  }
  for (let j = 0; j < 67; j++) {
    let gamesArrO = genGamesO(10000, '2022-05-01T01:01:00.000Z', '2023-05-25T00:00:00.000Z', 418, 2018);
    let gamesArrX = genGamesX(10000, '2022-05-01T01:01:00.000Z', '2023-05-25T00:00:00.000Z', 418, 2018);
    await knex('games').insert(gamesArrO);
    await knex('games').insert(gamesArrX);
  }
  for (let j = 0; j < 10; j++) {
    let gamesArrO = genGamesO(10000, '2023-03-01T01:01:00.000Z', '2023-03-31T00:00:00.000Z', 418, 618);
    let gamesArrX = genGamesX(10000, '2023-03-01T01:01:00.000Z', '2023-03-31T00:00:00.000Z', 418, 618);
    await knex('games').insert(gamesArrO);
    await knex('games').insert(gamesArrX);
  }
};
