/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
const { faker } = require('@faker-js/faker');

exports.seed = async function (knex) {
 
    const users = [];

    for (let i = 0; i < 2000; i++) {
      users.push({
        surName: faker.person.lastName(),
        firstName: faker.person.firstName(),
        secondName: faker.person.middleName(),
        age: faker.number.int({ min: 5, max: 113 }),
        gender: faker.datatype.boolean(),
        statusActive: faker.datatype.boolean(),
        statusGame: faker.datatype.boolean(),
        createdAt: faker.date.between({from: '2022-01-01T00:00:00.000Z', to: '2022-04-01T00:00:00.000Z'}),
        updatedAt: faker.date.between({from: '2022-05-01T00:00:00.000Z', to: '2023-04-01T00:00:00.000Z'})
      });
    };
    
  await knex('users').insert(users);
};
