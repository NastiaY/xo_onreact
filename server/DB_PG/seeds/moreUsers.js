/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').insert([
    {id: 11, surName: 'Василенко', firstName: 'Эрик', secondName: 'Платонович', 
    age: 22, gender: false, statusActive: true, statusGame: true, 
    createdAt: '2021-10-12 16:59:30.11', updatedAt: '2021-10-22 16:59:31.1'},
    {id: 12, surName: 'Быков', firstName: 'Юрий', secondName: 'Виталиевич', 
    age: 44, gender: false, statusActive: true, statusGame: true, 
    createdAt: '2021-10-12 16:59:30.12', updatedAt: '2021-10-22 16:59:31.2'},
    {id: 13, surName: 'Многогрешный', firstName: 'Павел', secondName: 'Виталиевич', 
    age: 24, gender: false, statusActive: false, statusGame: false, 
    createdAt: '2021-10-12 16:59:30.13', updatedAt: '2021-10-22 16:59:31.3'},
    {id: 14, surName: 'Галкин', firstName: 'Феликс', secondName: 'Платонович', 
    age: 32, gender: false, statusActive: true, statusGame: true, 
    createdAt: '2021-10-12 16:59:30.14', updatedAt: '2021-10-22 16:59:31.4'},
    {id: 15, surName: 'Шевченко', firstName: 'Рафаил', secondName: 'Михайлович', 
    age: 37, gender: false, statusActive: true, statusGame: true, 
    createdAt: '2021-10-12 16:59:30.15', updatedAt: '2021-10-22 16:59:31.5'},
    {id: 16, surName: 'Гордеев', firstName: 'Шамиль', secondName: 'Леонидович', 
    age: 39, gender: false, statusActive: true, statusGame: false, 
    createdAt: '2021-10-12 16:59:30.16', updatedAt: '2021-10-22 16:59:31.6'},
    {id: 17, surName: 'Суворов', firstName: 'Феликс', secondName: 'Григорьевич', 
    age: 25, gender: false, statusActive: true, statusGame: true, 
    createdAt: '2021-10-12 16:59:30.17', updatedAt: '2021-10-22 16:59:31.7'},
  ]);
};

