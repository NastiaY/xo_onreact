/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = function(knex) {
  // Deletes ALL existing entries

  return knex('users').insert([
    {id: 1, surName: 'Пупкин', firstName: 'Владлен', secondName: 'Игоревич', 
    age: 33, gender: false, statusActive: true, statusGame: true, 
    createdAt: '2021-10-12 16:59:30.01', updatedAt: '2021-10-22 16:59:30.1'},
    {id: 2, surName: 'Плюшкина', firstName: 'Екатерина', secondName: 'Викторовна', 
    age: 28, gender: true, statusActive: true, statusGame: true, 
    createdAt: '2021-10-12 16:59:30.02', updatedAt: '2021-10-22 16:59:30.2'},
    {id: 3, surName: 'Александров', firstName: 'Игнат', secondName: 'Анатолиевич', 
    age: 24, gender: false, statusActive: false, statusGame: false, 
    createdAt: '2021-10-12 16:59:30.03', updatedAt: '2021-10-22 16:59:30.3'},
    {id: 4, surName: 'Мартынов', firstName: 'Остап', secondName: 'Фёдорович', 
    age: 12, gender: false, statusActive: true, statusGame: false, 
    createdAt: '2021-10-12 16:59:30.04', updatedAt: '2021-10-22 16:59:30.4'},
    {id: 5, surName: 'Комаров', firstName: 'Цефас', secondName: 'Александрович', 
    age: 83, gender: false, statusActive: true, statusGame: true, 
    createdAt: '2021-10-12 16:59:30.05', updatedAt: '2021-10-22 16:59:30.5'},
    {id: 6, surName: 'Кулаков', firstName: 'Станислав', secondName: 'Петрович', 
    age: 43, gender: false, statusActive: false, statusGame: false, 
    createdAt: '2021-10-12 16:59:30.06', updatedAt: '2021-10-22 16:59:30.6'},
    {id: 7, surName: 'Борисов', firstName: 'Йошка', secondName: 'Васильевич', 
    age: 32, gender: false, statusActive: true, statusGame: false, 
    createdAt: '2021-10-12 16:59:30.07', updatedAt: '2021-10-22 16:59:30.7'},
    {id: 8, surName: 'Негода', firstName: 'Михаил', secondName: 'Эдуардович', 
    age: 33, gender: false, statusActive: true, statusGame: true, 
    createdAt: '2021-10-12 16:59:30.08', updatedAt: '2021-10-22 16:59:30.8'},
    {id: 9, surName: 'Жданов', firstName: 'Зураб', secondName: 'Алексеевич', 
    age: 24, gender: false, statusActive: true, statusGame: false, 
    createdAt: '2021-10-12 16:59:30.09', updatedAt: '2021-10-22 16:59:30.9'},
    {id: 10, surName: 'Бобров', firstName: 'Фёдор', secondName: 'Викторович', 
    age: 19, gender: false, statusActive: false, statusGame: false, 
    createdAt: '2021-10-12 16:59:30.101', updatedAt: '2021-10-22 16:59:31'},
  ]);
};


	