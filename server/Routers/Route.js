const Router = require('express');
const AuthController = require('../controllers/AuthController.js');
const RatingControl = require('../controllers/RatingController.js');
const GameController = require('../controllers/GameController.js');
const ActPlController = require('../controllers/ActivePlController.js');
const AllGamesController = require('../controllers/AllGamesController.js');
const AllPlController = require('../controllers/AllPlController.js');
const AuthMiddleware = require('../service/auth-middleware.js');
const GamesController = require('../controllers/GamesController.js');  
const ChatController = require('../controllers/ChatController.js'); 

const Routers = new Router();
const gameContrl = new GameController();

Routers.post('/registr', AuthController.registration);
Routers.post('/login', AuthController.login);
Routers.get('/order', AuthController.authenticateToken);
Routers.post('/logout', AuthController.logout);

Routers.get('/rating', AuthMiddleware, RatingControl.getRating);
Routers.get('/rating/:id', AuthMiddleware, RatingControl.getRatingPlayer);
Routers.get('/active', AuthMiddleware, ActPlController.getActPlayers);
Routers.get('/allgames', AuthMiddleware, AllGamesController.getAllGames);

Routers.get('/allplayers', AuthMiddleware, AllPlController.getAllPlayers);
Routers.post('/allplayers', AuthMiddleware, AllPlController.postAddPlayer);
Routers.put('/allplayers', AuthMiddleware, AllPlController.updatePlayer);
Routers.delete('/allplayers', AuthMiddleware, AllPlController.delPlayer);

Routers.post('/games', AuthMiddleware, GamesController.postNewGame);
Routers.get('/games', AuthMiddleware, GamesController.getGame);
Routers.put('/games', AuthMiddleware, GamesController.updateGame);
Routers.delete('/games', AuthMiddleware, GamesController.delGame);

Routers.put('/field', AuthMiddleware, GamesController.updateField);
Routers.get('/field', AuthMiddleware, GamesController.getField);

Routers.post('/chat', AuthMiddleware, ChatController.postMsg);
Routers.get('/chat', AuthMiddleware, ChatController.getChat);

// Для ДЗ 11. 
Routers.post('/game', AuthMiddleware, gameContrl.createField.bind(gameContrl));
Routers.get('/game', AuthMiddleware, gameContrl.getField.bind(gameContrl));
Routers.put('/game', AuthMiddleware, gameContrl.updateField.bind(gameContrl));
Routers.delete('/game', AuthMiddleware, gameContrl.delField.bind(gameContrl));

module.exports = Routers;