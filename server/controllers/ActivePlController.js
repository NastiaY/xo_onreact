const knexConfig = require('../DB_PG/knexfile.js');
const knex = require('knex')(knexConfig.development);


class ActPlController {

    async getActPlayers(req, res) {

        const {id, players} = req.query;

        if (id) {
            knex('users')
            .select('id', 'firstName', 'surName', 
            'secondName', 'statusGame')
            .where('statusActive', true)
            .whereNot('id', id)
            .whereIn('id', players || [])
            .then(rows => {
                return res.status(200).json(rows);
            })
            .catch((err) => {
                res.status(500).send(err);
            })
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют');
        };   
    };
};

module.exports = new ActPlController(); 