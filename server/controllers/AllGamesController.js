const knexConfig = require('../DB_PG/knexfile.js');
const knex = require('knex')(knexConfig.development);

class AllGamesController {

    async getAllGames(req, res) {

        const { id } = req.query;

        let result = knex('games')
            .select('games.id',
                'po.firstName as plO',
                'po.surName as plO_surName',
                'po.secondName as plO_secondName',
                'px.firstName as plX',
                'px.surName as plX_surName',
                'px.secondName as plX_secondName',
                'games.gameResult',
                'games.gameBegin as start',
                'games.gameEnd as end')
            .innerJoin('users as po', function () {this.on('po.id', '=', 'games.idPlayerO')})
            .innerJoin('users as px', function () {this.on('px.id', '=', 'games.idPlayerX')})
            .whereNotNull('gameEnd')
            .orderBy('gameEnd', 'desc')
            .limit(12)

            if (id) {
                result = result.where(function() {
                    this.where('games.idPlayerO', id).orWhere('games.idPlayerX', id);
                })
            };
            
            result.then(rows => {
                return res.status(200).json(rows);
            })
            .catch((err) => {
                res.status(500).send(err);
            });
    };
};

module.exports = new AllGamesController(); 