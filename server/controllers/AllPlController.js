const knexConfig = require('../DB_PG/knexfile.js');
const knex = require('knex')(knexConfig.development);

class AllPlController {

    async getAllPlayers(req, res) {

        const { id, limit, offset } = req.query;

        let result = knex('users')
            .orderBy('surName', 'asc')
            .orderBy('id', 'asc');
        
        let total = await knex('users').count('id')

        if (id) {
            result = result.where('id', id)
        };
        if (offset) {
            result = result.offset(offset);
        };
        if (limit) {
            result = result.limit(limit);
        };
        

        result.then(rows => {
            res.header('x-total-count', total[0].count)
            return res.status(200).json(rows);
        })
            .catch((err) => {
                res.status(500).send(err);
            });
    };

    async postAddPlayer(req, res) {

        const newPlayer = req.body;

        if (newPlayer) {
            knex('users')
                .insert(newPlayer)
                .then(() => {
                    res.status(201).send(`Игрок успешно добавлен`);
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    async updatePlayer(req, res) {

        const { id } = req.query;
        const upParams = req.body;

        if (id && upParams) {
            knex('users')
                .where('id', id)
                .update(upParams)
                .then(() => {
                    res.status(200).send('Игрок успешно обновлен');
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    // Для тестов
    async delPlayer(req, res) {

        const { id } = req.query;

        if (id) {
            knex('users')
                .delete()
                .where('id', id)
                .then(() => {
                    res.status(201).send(`Игрок успешно удален`);
                })
                .catch((err) => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };
};

module.exports = new AllPlController(); 