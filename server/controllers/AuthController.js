require('dotenv').config();
const knexConfig = require('../DB_PG/knexfile.js');
const knex = require('knex')(knexConfig.development);
const bcrypt = require('bcryptjs');
const tokenService = require('../service/token-service.js');


class AuthController {

    registration(req, res) { // Пока только через postman 

        const { id } = req.query;
        const { Login, Pass, Role } = req.body;

        knex('accounts').where('idUser', id)
            .then(rows => {
                if (rows.length) {
                    res.status(409).send('Аккаунт с таким id уже существует')
                } else {
                    bcrypt.hash(Pass, 10, (err, hash) => {
                        knex('accounts')
                            .insert({ idUser: id, login: Login, password: hash, role: Role })
                            .then(() => {
                                res.status(201).send('Аккаунт успешно создан')
                            });
                    });
                };
            })
            .catch(err => {
                res.status(500).send(err);
            })
    };

    login(req, res) {

        const { Login, Pass } = req.body;

        knex('accounts')
            .where('login', Login)
            .select('password', 'role', 'idUser', 'users.statusActive')
            .join('users', 'users.id', '=', 'accounts.idUser')
            .then(async rows => {
                if (!rows[0].statusActive) return res.status(403).send('Аккаунт заблокирован');
                if (rows.length) {
                    if (await bcrypt.compare(Pass, rows[0].password)) {
                        const { access_token, refresh_token } = tokenService.generateTokens({ id: rows[0].idUser, role: rows[0].role });
                        res.cookie('refresh_token', refresh_token, {
                            httpOnly: true,
                            secure: true,
                            sameSite: 'strict',
                            maxAge: 30 * 24 * 60 * 60 * 1000
                        })
                            .cookie('access_token', access_token, {
                                httpOnly: true,
                                secure: true,
                                sameSite: 'strict',
                                maxAge: 15 * 60 * 1000
                            });
                        return res.status(200).json({ id: rows[0].idUser, role: rows[0].role });
                    } else {
                        res.status(402).send(`Неверный пароль`);
                    }
                } else {
                    res.status(401).send(`Неверный логин`);
                }
            })
            .catch(err => {
                res.status(500).send(err);
            })
    };

    authenticateToken(req, res) {

        const refreshToken = req.cookies.refresh_token;

        if (!refreshToken) {
            return res.status(401).send('Токен аутентификации отсутствует');
        }

        const valRefToken = tokenService.validateRefreshToken(refreshToken);

        if (!valRefToken) {
            return res.status(401).send('Неверный токен аутентификации');
        }

        const { access_token, refresh_token } = tokenService.generateTokens({ id: valRefToken.id, role: valRefToken.role });
        
        res.cookie('refresh_token', refresh_token, {
            httpOnly: true,
            secure: true,
            sameSite: 'strict',
            maxAge: 30 * 24 * 60 * 60 * 1000
        })
            .cookie('access_token', access_token, {
                httpOnly: true,
                secure: true,
                sameSite: 'strict',
                maxAge: 15 * 60 * 1000
            });
        return res.status(200).json({ id: valRefToken.id, role: valRefToken.role });
    };

    async logout(req, res) {
        try {
            res.clearCookie('refresh_token');
            res.clearCookie('access_token');
            return res.status(200).send('Вы успешно вышли');
        } catch (err) {
            res.status(500).send(err);
        }
    };
}

module.exports = new AuthController();