const knexConfig = require('../DB_PG/knexfile.js');
const knex = require('knex')(knexConfig.development);

class ChatController {

    async postMsg(req, res) {

        if (req.body) {
            knex('messenger').insert(req.body)
            .then(() => {
                res.status(200).send('Сообщение успешно сохранено');
            })
            .catch(err => {
                res.status(500).send(err);
            })
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют');
        }
    };

    getChat(req, res) {

        const { id } = req.query;

        if (id) {
            knex('messenger')
                .where('idGame', id)
                .select('*')
                .orderBy('createdAt', 'asc')
                .then(rows => {
                    res.status(200).json(rows)
                })
                .catch(err => {
                    res.status(500).send(err);
                })
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют');
        };
    };
};

module.exports = new ChatController();