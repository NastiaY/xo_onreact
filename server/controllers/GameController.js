class GameController {

    constructor() {
        this.Game = [];
    };

    createField(req, res) {

        const { index } = req.query;

        if (req.body.field && index) {
            this.Game[index] = req.body.field;
            res.status(200).send('Поле сохранено');
        } else {
            res.status(400).send('Bad Request. Отсутствуют данные');
        }
    };

    getField(req, res) {

        const { index } = req.query;

        if (index && this.Game[index]) {
            res.status(200).send(this.Game[index]);
        } else {
            res.status(404).send('Not Found. Элемент не найден');
        }
    };

    updateField(req, res) {

        const { index } = req.query;

        if (req.body.field && index && this.Game[index]) {
            this.Game[index] = req.body.field;
            res.status(200).send('Поле обновлено');
        } else {
            res.status(404).send('Not Found. Такого поля нет');
        }
    };

    delField(req, res) {

        const { index } = req.query;

        if (index && this.Game[index]) {
            delete this.Game[index]
            res.status(200).send('Поле удалено');
        } else {
            res.status(404).send('Not Found. Элемент не найден');
        }
    };
};

module.exports = GameController;