const knexConfig = require('../DB_PG/knexfile.js');
const knex = require('knex')(knexConfig.development);

class GamesController {

    postNewGame(req, res) {

        if (req.body) {
            knex('games')
                .returning('id')
                .insert(req.body)
                .then((id) => {
                    res.status(200).json(id);
                })
                .catch(err => {
                    res.status(403).send(err)
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    getGame(req, res) {

        const { id } = req.query;

        if (id) {
            knex('games')
                .select('*')
                .where('idPlayerO', id)
                .orWhere('idPlayerX', id)
                .orderBy('id', 'desc')
                .first()
                .then(rows => {
                    res.status(200).json(rows);
                })
                .catch(err => {
                    res.status(500).send(err);
                });
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    async updateGame(req, res) {

        const { id } = req.query;

        if (id && req.body) {
            try {
                await knex('games')
                    .where('id', id)
                    .update(req.body)

                let resultX = await knex('games')
                    .select('idPlayerX as user_id',
                        knex.raw(`'x' as symbol`),
                        knex.raw(`date_trunc('month', "gameBegin") as period`),
                        knex.raw(`(CASE WHEN "gameResult" = FALSE THEN 1 ELSE 0 END) as wins`),
                        knex.raw(`(CASE WHEN "gameResult" = TRUE THEN 1 ELSE 0 END) as loses`),
                        knex.raw(`(CASE WHEN "gameResult" IS NULL THEN 1 ELSE 0 END) as draws`))
                    .where('id', id)
                let resultO = await knex('games')
                    .select('idPlayerO as user_id',
                        knex.raw(`'o' as symbol`),
                        knex.raw(`date_trunc('month', "gameBegin") as period`),
                        knex.raw(`(CASE WHEN "gameResult" = TRUE THEN 1 ELSE 0 END) as wins`),
                        knex.raw(`(CASE WHEN "gameResult" = FALSE THEN 1 ELSE 0 END) as loses`),
                        knex.raw(`(CASE WHEN "gameResult" IS NULL THEN 1 ELSE 0 END) as draws`))
                    .where('id', id)
                for (let row of [resultX[0], resultO[0]]) {
                    await knex('counters')
                        .insert(row)
                        .onConflict(['user_id', 'symbol', 'period'])
                        .merge({
                            wins: knex.raw(`counters.wins + EXCLUDED.wins`),
                            loses: knex.raw(`counters.loses + EXCLUDED.loses`),
                            draws: knex.raw(`counters.draws + EXCLUDED.draws`)
                        });
                }

                res.status(200).send('Игра обновлена');

            } catch (err) {
                res.status(500).send(err)
            }
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    delGame(req, res) { // Пока только для postman

        const { id } = req.query;

        if (id) {
            knex('games')
                .delete()
                .where('id', id)
                .then(() => {
                    res.status(200).send(`Игра ${id} удалена`);
                })
                .catch(err => {
                    res.status(500).send(err);
                })
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    async updateField(req, res) {

        const { id } = req.query;

        if (id && req.body.field) {
            try {
                await knex('games')
                    .where('id', id)
                    .update('field', JSON.stringify(req.body.field))
                    .then(() => {
                        res.status(200).send('Поле обновлено');
                    })
            } catch (err) {
                res.status(500).send(err);
            }
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

    async getField(req, res) {

        const { id } = req.query;

        if (id) {
            try {
                await knex('games')
                    .select('field')
                    .where('id', id)
                    .then(rows => {
                        res.status(200).json(rows[0].field);
                    })
            } catch (err) {
                res.status(500).send(err);
            }
        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        };
    };

};

module.exports = new GamesController();