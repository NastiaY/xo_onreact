const knexConfig = require('../DB_PG/knexfile.js');
const knex = require('knex')(knexConfig.development);

class RatingController {

    async getRating(req, res) {

        const { id, limit, offset } = req.query;

        let total = await knex('users').count('id').where('statusActive', true);

        let result = knex('counters')
            .select('counters.user_id as id', 'users.firstName', 'users.surName', 'users.secondName',
                knex.raw(`SUM(wins::float) * 100 / SUM(wins + loses + draws) AS wins_prc`),
                knex.raw(`SUM(wins * (CASE WHEN symbol = 'x' THEN 0.9 ELSE 1 END) - 
            loses * (CASE WHEN symbol = 'x' THEN 1.1 ELSE 1 END) + 
            draws * 0.25) / COUNT(DISTINCT period) AS rating`),
                knex.raw(`SUM(wins + loses + draws) as total`))
            .sum('wins AS wins')
            .sum('loses AS loses')
            .join('users', function () { this.on('counters.user_id', '=', 'users.id') })
            .whereRaw(`period >= date_trunc('month', now() - interval '6 month')`)
            .where('users.statusActive', true)
            .groupBy('user_id', 'users.firstName', 'users.surName', 'users.secondName')
            .having(knex.raw(`SUM(wins + loses + draws)`), '>', 50)
            .orderBy('rating', 'desc')

        if (id) {
            result = result.whereIn('user_id', id.split(','));
        }
        if (offset) {
            result = result.offset(offset);
        }
        if (limit) {
            result = result.limit(limit);
        }

        result.then(rows => {
            res.header('x-total-count', total[0].count)
            return res.status(200).json(rows);
        })
            .catch((err) => {
                res.status(500).send(err);
            });
    };

    async getRatingPlayer(req, res) {

        const id = req.params.id;

        if (id) {
            try {
                knex('counters')
                    .select('counters.user_id as id', 'users.firstName', 'users.surName', 'users.secondName',
                        knex.raw(`SUM(wins::float) * 100 / SUM(wins + loses + draws) AS wins_prc`))
                    .join('users', function () { this.on('counters.user_id', '=', 'users.id') })
                    .groupBy('user_id', 'users.firstName', 'users.surName', 'users.secondName')
                    .whereIn('user_id', id.split(','))
                    .then(rows => {
                        if (!rows.length) {
                            knex('users')
                                .select('id', 'firstName', 'surName', 'secondName')
                                .where('id', id)
                                .then(row => {
                                    return res.status(200).json(row);
                                })
                        } else {
                            return res.status(200).json(rows);
                        }
                    })
            } catch (err) {
                res.status(500).send(err);
            }

        } else {
            res.status(400).send('Bad Request. Данные отсутствуют')
        }
    };

};

module.exports = new RatingController(); 