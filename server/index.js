require('dotenv').config()
const express = require('express');
const https = require('https');
const Routers = require('./Routers/Route.js');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const App = express();
const WebSocketServer = require('./websocket.js');
const fs = require('fs');


const PORT = process.env.PORT || 5000;

const options = {
    key: fs.readFileSync('./wssData/server.key'),
    cert: fs.readFileSync('./wssData/server.crt')
};

const server = https.createServer(options, App);

App.use(express.json());
App.use(cookieParser());
App.use(cors({
    origin: process.env.FRONT_URL,
    optionsSuccessStatus: 200,
    credentials: true,
    exposedHeaders: ['x-total-count']
}));
App.use('/api', Routers);

WebSocketServer(server);
    

const startApp = async () => {
    try {
        server.listen(PORT, () => console.log('Сервер запущен, порт=' + PORT));
    } catch (e) {
        console.log(e);
    }
}

startApp();


