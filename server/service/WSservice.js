
class WSservice {

    players;

    constructor(wss) {
        this.wss = wss
        this.players = new Set();
    };

    gameConnection = (ws, msg) => {
        ws.id = msg.id;
        this.connectionHandler(ws, msg);
    };

    connectionHandler = (ws, msg) => {
        this.wss.clients.forEach(client => {
            if (client.id === msg.id) {
                client.send(JSON.stringify(msg));
            };
        });
    };

    connect = (ws, msg) => {
        ws.userId = msg.userId;
        this.players.add(msg.userId);
        this.wss.clients.forEach(client => {
            client.send(JSON.stringify({
                method: 'connect',
                players: [...this.players]}));
        });
    };

    gameHandler = (ws, msg) => {
        this.wss.clients.forEach(client => {
            if (client.userId === msg.userId) {
                client.send(JSON.stringify(msg));
            };            
        });
    };

    disconnect = (ws, msg) => {
        this.players.delete(msg.userId);
        this.wss.clients.forEach(client => {
            client.send(JSON.stringify({
                method: 'disconnect',
                players: [...this.players]
            }));
        })

    };
};

module.exports = WSservice;