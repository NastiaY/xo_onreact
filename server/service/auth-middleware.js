const tokenService = require("./token-service");

module.exports = function(req, res, next) {
    try {
        const accessToken = req.cookies.access_token;
        if (!accessToken) {
            return res.status(401).send('Токен аутентификации отсутствует');
        }
        const userData = tokenService.validateAccessToken(accessToken);                
        if (!userData) {
            return res.status(401).send('Неверный токен аутентификации');
        }
        req.user = userData;
        next();        
    } catch(err) {
        return res.status(401).send('Пользователь не авторизован');
    };
};