require('dotenv').config();
const jwt = require('jsonwebtoken');

class TokenService {

    generateTokens(payload) {
        const access_token = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '15m' });
        const refresh_token = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, { expiresIn: '30d' });
        return {
            access_token,
            refresh_token
        }
    };

    validateAccessToken(token) {
        try {
            const userData = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
            return userData;
        } catch (err) {
            return null;
        }
    };

    validateRefreshToken(token) {
        try {
            const userData = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET);
            return userData;
        } catch (err) {
            return null;
        }
    };
};

module.exports = new TokenService();