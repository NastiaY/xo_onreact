const { WebSocketServer } = require('ws');
const WSservice = require('./service/WSservice.js');

module.exports = (server) => {
    const wss = new WebSocketServer({ server });
    const wsServ = new WSservice(wss);
    
    wss.on('connection', (ws) => {
        console.log('Веб-сокет подрубился')
        ws.on('message', (msg) => {
            msg = JSON.parse(msg);
            switch (msg.method) {
                case "game":
                    wsServ.connectionHandler(ws, msg);
                    break;
                case "gameConnect":
                    wsServ.gameConnection(ws, msg);
                    break;
                case 'connect':
                    wsServ.connect(ws, msg);
                    break;
                case 'invite':
                    wsServ.gameHandler(ws, msg);
                    break;
                case 'answer':
                    wsServ.gameHandler(ws, msg);
                    break;
                case 'chat':
                    wsServ.connectionHandler(ws, msg);
                    break;
                case 'no':
                    wsServ.gameHandler(ws, msg);
                    break;
                case 'disconnect':
                    wsServ.disconnect(ws, msg);
                    break;
                case 'giveup':
                    wsServ.connectionHandler(ws, msg);
                    break;
                default:
                    console.log('Неизвестный метод');
            }
        });
    });   
};

