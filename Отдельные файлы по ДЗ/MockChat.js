import { action, computed, makeAutoObservable, observable } from 'mobx';
import { makePersistable, clearPersistedStore } from "mobx-persist-store";

class MockChatApi {

    messages = [];
    _job;

    constructor() {

        makeAutoObservable(this, {
            messages: observable,
            sendMessage: action,
            emitHi: action,
            messageSent: action,
            newChat: action,
            moment: computed,
        });
        makePersistable(this, {
            name: 'MockChatStore',
            properties: ['messages'],
            storage: window.localStorage,
            expireIn: 86400000,
        });
    };

    sendMessage(mess) {
        this.messages = [...this.messages, mess];
        this.messageSent('👍');
    };

    someoneHi(message) {
        setTimeout(() => {
            this.messages = [...this.messages, {
                id: Date.now(),
                name: 'Владлен Пупкин',
                time: this.moment,
                role: 'o',
                type: "send-message",
                message
            }];
        }, 4000);
    };

    someoneSent(message) {
        if (this._job) {
            clearTimeout(this._job);
        };
        this._job = setTimeout(() => {
            this.messages = [...this.messages, {
                id: Date.now(),
                name: 'Владлен Пупкин',
                time: this.moment,
                role: 'o',
                type: 'send-message',
                message
            }];
            this.someoneSent('Ты тут?');
        }, 15000)
    };

    messageSent(message) {
        if (this._job) {
            clearTimeout(this._job);
        };
        this._job = setTimeout(() => {
            this.messages = [...this.messages, {
                id: Date.now(),
                name: 'Владлен Пупкин',
                time: this.moment,
                role: 'o',
                type: 'send-message',
                message
            }];
            this.someoneSent('Ты тут?');
        }, 2000);
    };

    get moment() {
        var now = new Date();
        var moment = now.getHours() + ':' + String(now.getMinutes()).padStart(2, '0');
        return moment;
    };

    newChat() {
        if (this._job) {
            clearTimeout(this._job);
        };
        this.messages = [];
    }

};

export default new MockChatApi();