const mockApiCall = (data) => 
    new Promise((resolve, reject) => {
        setTimeout(() => {
            if (Math.random() < 0.1) {
                reject(new Error());
            } else {
                resolve(data);
            }
        }, Math.random() * 4000);
    });


class MockGame {

    mockPlayers = [
        {
            id: 1,
            name: 'Владлен',
            surname: 'Пупкин',
            patro: 'Игоревич',
            role: 'o',
            wins: '63% побед'
        },
        {
            id: 2,
            name: 'Екатерина',
            surname: 'Плюшкина',
            patro: 'Викторовна',
            role: 'x',
            wins: '23% побед'
        }];

    
    getPlayersInfo() {
        const data = mockApiCall(this.mockPlayers);
        return data;
    }
};

export default MockGame;