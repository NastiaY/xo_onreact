SELECT user_id, symbol, 
EXTRACT(YEAR FROM period) AS year,
EXTRACT(MONTH FROM period) AS month,
SUM(wins) AS wins,
SUM(loses) AS loses,
SUM(draws) AS draws,
SUM(wins::float) * 100 / SUM(wins + loses + draws) AS wins_prc,
SUM(wins + loses + draws) AS total,
SUM(wins * (CASE WHEN symbol = 'x' THEN 0.9 ELSE 1 END) -
   loses * (CASE WHEN symbol = 'x' THEN 1.1 ELSE 1 END) + draws * 0.25) AS rating
FROM counters
WHERE user_id = 455
GROUP BY user_id, year, CUBE(symbol, EXTRACT(MONTH FROM period)) 
ORDER BY month, year, symbol ASC;



