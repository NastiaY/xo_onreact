WITH counters_2 AS (SELECT u.id AS user_id,
				   'x' AS symbol,
				   date_trunc('month', g."gameBegin") AS period,
				   COUNT(g.id) FILTER (WHERE g."gameResult" = FALSE) AS wins,
				   COUNT(g.id) FILTER (WHERE g."gameResult" = TRUE) AS loses,
				   COUNT(g.id) FILTER (WHERE g."gameResult" IS NULL) AS draws
				   FROM users AS u
				   JOIN games AS g ON u.id = g."idPlayerX"
				   GROUP BY u.id, date_trunc('month', g."gameBegin")
				   UNION ALL
				   SELECT u.id AS user_id,
				   'o' AS symbol,
				   date_trunc('month', g."gameBegin") AS period,
				   COUNT(g.id) FILTER (WHERE g."gameResult" = TRUE) AS wins,
				   COUNT(g.id) FILTER (WHERE g."gameResult" = FALSE) AS loses,
				   COUNT(g.id) FILTER (WHERE g."gameResult" IS NULL) AS draws
				   FROM users AS u
				   JOIN games AS g ON u.id = g."idPlayerO"
				   GROUP BY u.id, date_trunc('month', g."gameBegin"))
SELECT user_id, users."surName", users."firstName", users."secondName",
SUM(wins + loses + draws) AS total,
SUM(wins) AS wins,
SUM(loses) AS loses,
SUM(wins::float) / SUM(wins + loses + draws) AS wins_prc,
SUM(wins * (CASE WHEN symbol = 'x' THEN 0.9 ELSE 1 END) -
   loses * (CASE WHEN symbol = 'x' THEN 1.1 ELSE 1 END) + draws * 0.25) /
   (COUNT(DISTINCT period)) AS rating
FROM counters_2
JOIN users ON counters_2.user_id = users.id
WHERE period >= date_trunc('month', now() - interval '6 month')
AND users."statusActive" = true 
GROUP BY user_id, users."surName", users."firstName", users."secondName"
HAVING SUM(wins + loses + draws) > 50
ORDER BY rating DESC;