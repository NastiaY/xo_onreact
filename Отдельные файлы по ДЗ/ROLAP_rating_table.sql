SELECT user_id, users."surName", users."firstName", users."secondName",
SUM(wins + loses + draws) AS total,
SUM(wins) AS wins,
SUM(loses) AS loses,
SUM(wins::float) * 100 / SUM(wins + loses + draws) AS wins_prc,
SUM(wins * (CASE WHEN symbol = 'x' THEN 0.9 ELSE 1 END) -
   loses * (CASE WHEN symbol = 'x' THEN 1.1 ELSE 1 END) + draws * 0.25) /
   (COUNT(DISTINCT period)) AS rating
FROM counters
JOIN users ON counters.user_id = users.id
WHERE period >= date_trunc('month', now() - interval '6 month')
AND users."statusActive" = true 
GROUP BY user_id, users."surName", users."firstName", users."secondName"
HAVING SUM(wins + loses + draws) > 50
ORDER BY rating DESC;